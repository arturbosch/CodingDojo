def fibonacci(number):
    if number < 0:
        return [1]
    if number == 1:
        return [1]
    if number == 2:
        return [1, 1]
    before_last, last = 1, 1
    sequence = [1, 1]
    while number - 2 > 0:
        # logic
        new = last + before_last
        sequence.append(new)
        before_last, last = last, new
        number -= 1
    return sequence


print(fibonacci(12))
