/**
 * @author Artur Bosch
 */
fun sam(n: Int, e: Int, m: Int): Int {
	var binExponent = java.lang.Integer.toBinaryString(e)
	binExponent = binExponent.substring(1) // erste 1 weglassen, da result = n
	var result = n
	for (bit in binExponent) {
		result = (result * result) % m
		print("SQ")
		if (bit == '1') {
			print("M")
			result = (result * n) % m
		}
	}
	return result
}

fun main(args: Array<String>) {
	// 79 is  in binary
	// Zu U8 Aufg 3 a b c Formel umstellen? c gar nicht
	// Zu 4a) was fuer Exponenten fuer jeden Schritt
	// Zu 4b) allgemein Komplexitaet zu unserem Algorithmus oder Anzahl der M's vergleichen
	println(sam(2, 79/*1001111*/, 101)) // 42, 6xSQ + 4xM = 10M vs 79M
	println(sam(3, 197/*11000101*/, 101)) // 15, 3xM + 7xSQ = 10M vs 197M
}
