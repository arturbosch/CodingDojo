import com.sun.net.httpserver.HttpServer
import java.net.InetSocketAddress

/**
 * @author Artur Bosch
 */
fun main(args: Array<String>) {
	HttpServer.create(InetSocketAddress(8008), 0).apply {
		createContext("/hello") { exchange ->
			exchange.responseHeaders.add("Content-type", "text/plain")
			exchange.sendResponseHeaders(200, 0)
			exchange.responseBody.use {
				it.write("Hello ${exchange.remoteAddress.hostName}".toByteArray())
			}
		}
		start()
	}
}
